<?php

namespace Dreas\Eav\Interfaces;


interface TypeInterface
{
    public static function getType($type);

    public static function setType($type);
}