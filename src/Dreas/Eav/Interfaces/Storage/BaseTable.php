<?php

namespace Dreas\Eav\Interfaces\Storage;


interface BaseTable
{
    public function check();

    public function create(array $cols, string $coment = '');

    public function drop();

    public function update(array $cols);
}