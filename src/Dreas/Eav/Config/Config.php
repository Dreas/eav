<?php

namespace Dreas\Eav\Config;


class Config
{
    /**
     * Save PDO object to library
     * @var array
     */
    private static $conf = [];

    /**
     * Get PDO object from static
     * @return \Psr\Container\ContainerInterface
     */
    public static function getContainer(){
        return static::$conf;
    }

    /**
     * SevaPDO object from static
     * @param $conf
     */
    public static function setContainer(\Psr\Container\ContainerInterface $container){
        static::$conf = $container;
    }
}