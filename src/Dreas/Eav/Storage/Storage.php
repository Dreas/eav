<?php

namespace Dreas\Eav\Storage;

use Dreas\Eav\Config\Config;
use Dreas\Eav\Exceptions\EavException;
use Dreas\Eav\Helpers\Helpers;
use Dreas\Eav\Sheme;

class Storage
{
    private $db;
    private $idEntity;
    private $prefixTable = 'eav_';

    /**
     * Storage constructor.
     * @throws EavException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct()
    {
        if(!Config::getContainer()->has('Eav_Config')){
            throw new EavException("Eav_Config not found");
        }
        $this->db = Config::getContainer()->get('Eav_Config');

        $this->checkEavStructure();
    }

    public function build(Sheme $eav_object)
    {
        $entityName = $eav_object->getEntity();
        $entityType = $eav_object->getType();

        // TODO: code build this
    }

    /**
     * Проверяет есть ли нужные таблицы в БД. Если их нет создаёт.
     */
    private function checkEavStructure()
    {
        $tableEntityType = new BaseTables($this->db, $this->prefixTable . 'entity_type');
        if(!$tableEntityType->check()){
            $colums = [
                [
                    'name' => 'id',
                    'type' => 'INT',
                    'signed' => false, // true = 'SIGNED', false = 'UNSIGNED', not use param = break this param
                    'null' => false, // true = 'NULL', false = 'NOT NULL'
                    'auto_increment' => true, // true = 'AUTO_INCREMENT', false or not use param = break this param
                    'primary' => true, // Create primary key on this name
                ],
                [
                    'name' => 'type',
                    'type' => 'CHAR(255)',
                    'null' => true,
                    'default' => '0', // string = 'your string', '' or false = not create default string
                ],
                [
                    'name' => 'name',
                    'type' => 'CHAR(255)',
                    'null' => true,
                    'default' => '0', // string = 'your string', '' or false = not create default string
                ],
                [
                    'name' => 'attribute',
                    'type' => 'CHAR(255)',
                    'null' => true,
                    'default' => '0', // string = 'your string', '' or false = not create default string
                ],
                [
                    'name' => 'attribute_type',
                    'type' => 'CHAR(255)',
                    'null' => true,
                    'default' => '0', // string = 'your string', '' or false = not create default string
                ],
            ];

            try {
                $tableEntityType->create($colums, 'Таблица хранит типы и названия атрибутов объектов сущностей для админки');
            } catch (EavException $e) {
                Helpers::showMessage($e->getMessage());
            }
        }
        $tableEntity = new BaseTables($this->db, $this->prefixTable . 'entity');
        if(!$tableEntity->check()){
            $colums = [
                [
                    'name' => 'id',
                    'type' => 'INT',
                    'signed' => false, // true = 'SIGNED', false = 'UNSIGNED', not use param = break this param
                    'null' => false, // true = 'NULL', false = 'NOT NULL'
                    'auto_increment' => true, // true = 'AUTO_INCREMENT', false or not use param = break this param
                    'primary' => true, // Create primary key on this name
                ],
                [
                    'name' => 'name',
                    'type' => 'CHAR(255)',
                    'null' => true,
                ],
                [
                    'name' => 'code',
                    'type' => 'CHAR(255)',
                    'null' => true,
                ],
                [
                    'name' => 'data_create',
                    'type' => 'TIMESTAMP',
                    'null' => true,
                ],
            ];

            try {
                $tableEntity->create($colums, 'Таблица хранит сущности');
            } catch (EavException $e) {
                Helpers::showMessage($e->getMessage());
            }
        }
        $tableAttribute = new BaseTables($this->db, $this->prefixTable . 'attribute');
        if(!$tableAttribute->check()){
            $colums = [
                [
                    'name' => 'id',
                    'type' => 'INT',
                    'signed' => false, // true = 'SIGNED', false = 'UNSIGNED', not use param = break this param
                    'null' => false, // true = 'NULL', false = 'NOT NULL'
                    'auto_increment' => true, // true = 'AUTO_INCREMENT', false or not use param = break this param
                    'primary' => true, // Create primary key on this name
                ],
                [
                    'name' => 'name',
                    'type' => 'CHAR(255)',
                    'null' => true,
                ],
                [
                    'name' => 'code',
                    'type' => 'CHAR(255)',
                    'null' => true,
                ],
            ];

            try {
                $tableAttribute->create($colums, 'Таблица хранит атрибуты');
            } catch (EavException $e) {
                Helpers::showMessage($e->getMessage());
            }
        }
        $tableValue = new BaseTables($this->db, $this->prefixTable . 'value');
        if(!$tableValue->check()){
            $colums = [
                [
                    'name' => 'id',
                    'type' => 'INT',
                    'signed' => false, // true = 'SIGNED', false = 'UNSIGNED', not use param = break this param
                    'null' => false, // true = 'NULL', false = 'NOT NULL'
                    'auto_increment' => true, // true = 'AUTO_INCREMENT', false or not use param = break this param
                    'primary' => true, // Create primary key on this name
                ],
                [
                    'name' => 'id_attribute',
                    'type' => 'INT',
                    'signed' => false,
                    'null' => false,
                ],
                [
                    'name' => 'value',
                    'type' => 'CHAR(255)',
                    'null' => false,
                ],
            ];

            try {
                $tableValue->create($colums, 'Таблица хранит значения аттрибутов');
            } catch (EavException $e) {
                Helpers::showMessage($e->getMessage());
            }
        }
    }
}