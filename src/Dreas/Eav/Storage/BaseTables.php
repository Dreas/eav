<?php

namespace Dreas\Eav\Storage;


use Dreas\Eav\Exceptions\EavException;
use Dreas\Eav\Interfaces\Storage\BaseTable;
use PDO;

class BaseTables implements BaseTable
{
    private $db;
    private $tableName;

    public function __construct(PDO $db, string $tableName)
    {
        $this->db = $db;
        $this->tableName = $tableName;
    }

    /**
     * паррамметр $cols принимает массив массивов с парамметрами для колонки таблицы. Пример:
     *
     * $cols = [
     *        [
     *            'name' => 'id', // Имя столбца
     *            'type' => 'INT', // Тип для mysql (Пока только так)
     *            'signed' => false, // true = 'SIGNED', false = 'UNSIGNED', not use param = break this param
     *            'null' => false, // true = 'NULL', false = 'NOT NULL'
     *            'auto_increment' => true, // true = 'AUTO_INCREMENT', false or not use param = break this param
     *            'default' => '0', // string = 'your string', '' or false = not create default string
     *            'primary' => true, // Create primary key on this name
     *        ],
     *        [
     *            'name' => 'type',
     *            'type' => 'CHAR(255)',
     *            'null' => true,
     *        ],
     * ]
     *
     * @param string $tableName
     * @param array $cols
     * @throws EavException
     */
    public function create(array $cols, string $coment = '')
    {
        $sql = "CREATE TABLE `" . $this->tableName ."` " . $this->getColOnArray($cols, $coment);
        $this->db->query($sql);
    }

    private function getColOnArray (array $arr, string $comment = '')
    {
        $primary_key = '';
        $res = "(";
        foreach ($arr as $key_col => $col){
            foreach ($col as $key => $param){
                switch ($key){
                    case 'name':
                        $res .= "`" . $param . "` ";
                        break;
                    case 'type':
                        $res .= $param . " ";
                        break;
                    case 'signed':
                        $res .= ($param === false) ? "UN" : '';
                        $res .= "SIGNED ";
                        break;
                    case 'null':
                        $res .= ($param === false) ? 'NOT ' : '';
                        $res .=  "NULL ";
                        break;
                    case 'auto_increment':
                        $res .= ($param === true) ? "AUTO_INCREMENT " : '';
                        break;
                    case 'default':
                        $res .= "DEFAULT '" . $param . "' ";
                        break;
                    case 'primary':
                        if($param === true){
                            $primary_key = $col['name'];
                        }
                        break;
                }
            }
            $res = rtrim($res);
            $res .= ',';
        }
        if(strlen($primary_key) > 0){
            $res .= "PRIMARY KEY (`" . $primary_key . "`)";
        } else {
            $res = rtrim($res, ',');
        }
        $res .=  ")";
        if($comment != ''){
            $res .= "COMMENT='" . $comment . "'";
        }
         return $res;
    }

    /**
     * Проверка на существование таблицы
     * @return bool
     */
    public function check()
    {
        try {
            $sql = 'SELECT 1 FROM ' . $this->tableName . ' LIMIT 1';
            $result = $this->db->query($sql);
        } catch (EavException $e){
            return false;
        }
        return $result !== false;
    }

    /**
     * Удаление таблицы
     */
    public function drop()
    {
        $sql = "DROP TABLE `" . $this->tableName . "`";
        $this->db->query($sql);
    }

    /**
     * Обновление таблицы
     * @param array $cols
     */
    public function update(array $cols)
    {
        // TODO: Implement update() method.
    }
}