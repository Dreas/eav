<?php

namespace Dreas\Eav;

use Dreas\EAV\Helpers\Helpers;
use Dreas\Eav\Storage\Storage;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class Sheme
{
    private $name;
    private $entityType;
    private $attributes = [];
    private $attribute;
    private $mode;

    const MODE_ADD = 1;
    const MODE_UPDATE = 2;
    const MODE_DELETE = 3;

    public function __construct(string $type, string $name)
    {
        $this->setType($type);
        $this->name = $name;
    }

    public function getType()
    {
        return $this->entityType;
    }

    public function setType($type)
    {
        $this->entityType = $type;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function addEntity()
    {
        $this->mode = $this::MODE_ADD;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->name;
    }

    /**
     * @param string $attribute
     * @return $this
     */
    public function addAtribute(string $attribute, $type, $defaultValue = false)
    {
        $this->attribute = $attribute;
        $this->attributes[$this->attribute] = [
            'name' => $this->attribute,
            'type' => $type,
            'default' => $defaultValue,
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function addValue($value, $code = false)
    {
        $this->attributes[$this->attribute]['values'][] = [
            'value' => $value,
            'code' => $code
        ];

        return $this;
    }

    public function commit()
    {
        try {
            $entity = new Storage();
            $entity->build($this);
        } catch (Exceptions\EavException $e) {
            Helpers::showMessage($e->getMessage());
        } catch (NotFoundExceptionInterface $e) {
            Helpers::showMessage($e->getMessage());
        } catch (ContainerExceptionInterface $e) {
            Helpers::showMessage($e->getMessage());
        }
    }
}