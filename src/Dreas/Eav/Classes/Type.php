<?php

namespace Dreas\Eav\Classes;

use Dreas\Eav\Interfaces\TypeInterface;

class Type implements TypeInterface
{
    private static $type = [
        'STRING' => 'string',
        'INT' => 'integer'
    ];

    public static function getType($type)
    {
        if(array_key_exists($type, self::$type)){
            return self::$type[$type];
        } else {
            return false;
        }
    }

    public static function setType($type)
    {
        // TODO: Implement setType() method.
    }
}